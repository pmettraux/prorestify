/* globals describe, it, beforeEach, afterEach */
'use strict';

const ProRESTify = require('../lib/prorestify');
const should = require('should');
const _ = require('lodash');
const winston = require('winston');
const sinon = require('sinon');
const express = require('express');
const BPromise = require('bluebird');
const joi = require('joi');
const rp = require('request-promise');
const bodyParser = require('body-parser');


describe('ProRESTify', () => {
  let sandbox;
  let stubs = {};
  let spies = {};
  const defaultIdRegexp = /[0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
  const defaultExpressRegExpString = '(' + defaultIdRegexp.toString().slice(1, -1) + ')';

  beforeEach(() => {
		sandbox = sinon.sandbox.create();
    stubs.winstonWarn = sandbox.stub(winston, 'warn', function(){
      // remove log to avoid spam during the tests
    });
    stubs.winstonInfo = sandbox.stub(winston, 'info', function(){
      // remove log to avoid spam during the tests
    });
    spies.hookBeforeMiddlewares = sandbox.spy(ProRESTify.prototype, 'hookBeforeMiddlewares');
    spies.hookBeforeValidation = sandbox.spy(ProRESTify.prototype, 'hookBeforeValidation');
    spies.hookBeforeController = sandbox.spy(ProRESTify.prototype, 'hookBeforeController');
    spies.hookSuccess = sandbox.spy(ProRESTify.prototype, 'hookSuccess');
    spies.hookFailure = sandbox.spy(ProRESTify.prototype, 'hookFailure');
  });

  afterEach(() => {
		sandbox.restore();
	});

  describe('Initialization', () => {
    it('should initialize the library and use the default options if none are given', () => {
      let rp = new ProRESTify();
      should(rp.options).eql({
        routeFolder: __dirname + '/routes/',
        allowUnknown: false,
        idRegExp: defaultIdRegexp,
        abortEarly: false,
        validate: BPromise.promisify(joi.validate)
      });
    });

    it('should initialize the library and merge the given options over the default ones', () => {
      function customValidate(a,b) {console.log(a,b);}

      let rp = new ProRESTify({
        routeFolder: __dirname + '/routes/',
        allowUnknown: true,
        abortEarly: true,
        validate: customValidate
      });

      should(rp.options).eql({
        routeFolder: __dirname + '/routes/',
        allowUnknown: true,
        abortEarly: true,
        idRegExp: /[0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
        validate: customValidate
      });
    });

    it('should initialize the library and merge the given options over the default ones with not all of them', () => {
      let rp = new ProRESTify({
        routeFolder: __dirname + '/routes/',
        allowUnknown: true
      });

      should(rp.options).eql({
        routeFolder: __dirname + '/routes/',
        allowUnknown: true,
        abortEarly: false,
        idRegExp: /[0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/,
        validate: BPromise.promisify(joi.validate)
      });
    });

    it('should throw an error if routeFolder does not exist', () => {
      try {
        new ProRESTify({
          routeFolder: '/unexistingFolder/',
        });
        throw 'Should have thrown an error before this line';
      } catch(error) {
        should(error).eql('ProRESTify - the folder: /unexistingFolder/ does not exist');
      }
    });

    [
      {
        key: 'routeFolder',
        value: 1234,
        description: 'not a string',
        error: '"routeFolder" must be a string',
        keyError: 'routeFolder'
      },
      {
        key: 'allowUnknown',
        value: 'true',
        description: 'not a bollean',
        error: '"allowUnknown" must be a boolean',
        keyError: 'allowUnknown'
      },
      {
        key: 'abortEarly',
        value: 'true',
        description: 'not a bollean',
        error: '"abortEarly" must be a boolean',
        keyError: 'abortEarly'
      },
      {
        key: 'idRegExp',
        value: true,
        description: 'not a regexp',
        error: '"idRegExp" must be an object',
        keyError: 'idRegExp'
      },
      {
        key: 'idRegExp',
        value: 123,
        description: 'not a regexp',
        error: '"idRegExp" must be an object',
        keyError: 'idRegExp'
      },
      {
        key: 'idRegExp',
        value: {},
        description: 'not a regexp',
        error: '"idRegExp" must be an instance of "RegExp"',
        keyError: 'idRegExp'
      },
      {
        key: 'validate',
        value: {},
        description: 'not a function',
        error: '"validate" must be a Function',
        keyError: 'validate'
      },
      {
        key: 'validate',
        value: 123,
        description: 'not a function',
        error: '"validate" must be a Function',
        keyError: 'validate'
      },
      {
        key: 'validate',
        value: '123',
        description: 'not a function',
        error: '"validate" must be a Function',
        keyError: 'validate'
      },
      {
        key: 'validate',
        value: function(){},
        description: 'not a function',
        error: '"validate" must have an arity greater or equal to 2',
        keyError: 'validate'
      },
      {
        key: 'validate',
        value: function(a,b,c,d){console.log(a,b,c,d);},
        description: 'not a regexp',
        error: '"validate" must have an arity lesser or equal to 3',
        keyError: 'validate'
      }
    ].forEach(item => {
      it('should throw an error if ' + item.key + ' is ' + item.description, () => {
        try {
          let options = {};
          options[item.key] = item.value;
          new ProRESTify(options);
          throw 'Should have thrown an error before this line';
        } catch(error) {
          should(error.details[0].path).eql(item.keyError);
          should(error.details[0].message).eql(item.error);
        }
      });
    });
  });

  describe('Routes Generation', () => {
    let app;

    beforeEach(() => {
      app = express();
    });

    it('should log a warning if no endpoints are detected', () => {
      let routeFolder = __dirname + '/emptyFolder/';
      new ProRESTify({
        routeFolder
      }).router(app);

      should(stubs.winstonWarn.callCount).eql(1);
      should(stubs.winstonWarn.lastCall.args[0]).eql('ProRESTify - folder:' + routeFolder + ' is empty. No endpoints detected');
    });

    it('should log a warning if content is missing within the file', () => {
      let routeFolder = __dirname + '/missingContent/';
      new ProRESTify({
        routeFolder
      }).router(app);

      should(stubs.winstonWarn.callCount).eql(2);
      should(stubs.winstonWarn.firstCall.args[0]).eql('ProRESTify - No endpoints were found in this existing file:' + routeFolder + 'wrong/wrong.js');
      should(stubs.winstonWarn.secondCall.args[0]).eql('ProRESTify - No endpoints were found in this existing file:' + routeFolder + 'wrong.js');
    });

    it('should log a warning if version is missing from the folder structure', () => {
      let routeFolder = __dirname + '/missingVersion/';
      new ProRESTify({
        routeFolder
      }).router(app);

      should(stubs.winstonWarn.callCount).eql(16);
      should(stubs.winstonWarn.firstCall.args[0]).eql('ProRESTify - version is missing for file:' + routeFolder + 'wrong/wrong.js action:create cannot retrieve the endpoint');
      should(stubs.winstonWarn.secondCall.args[0]).eql('ProRESTify - version is missing for file:' + routeFolder + 'wrong/wrong.js action:list cannot retrieve the endpoint');
      should(stubs.winstonWarn.thirdCall.args[0]).eql('ProRESTify - version is missing for file:' + routeFolder + 'wrong/wrong.js action:get cannot retrieve the endpoint');
    });

    it('should log a warning if the action type is unknown', () => {
      let routeFolder = __dirname + '/unknownAction/';
      new ProRESTify({
        routeFolder
      }).router(app);

      should(stubs.winstonWarn.callCount).eql(1);
      should(stubs.winstonWarn.firstCall.args[0]).eql('ProRESTify - Unknown action type for file:' + routeFolder + 'v1/wrong.js. No endpoint was created');
    });

    it('should flatten the folder structure and retrieve the endpoints', () => {
      let prorestify = new ProRESTify();
      prorestify.router(app);

      // oder is important
      let expectedRoutes = [
        {url: '/v1/camel-case', method: 'GET', isCustomAction: false},
        {
          url: '/v1/users/:userId' + defaultExpressRegExpString + '/children/:childId' + defaultExpressRegExpString + '/friends',
          method: 'POST',
          isCustomAction: false
        },
        {url: '/v1/users', method: 'POST', isCustomAction: false},
        {url: '/v1/users', method: 'GET', isCustomAction: false},
        {url: '/v1/users/:userId' + defaultExpressRegExpString, method: 'GET', isCustomAction: false},
        {url: '/v1/users/:userId' + defaultExpressRegExpString, method: 'PUT', isCustomAction: false},
        {url: '/v1/users/:userId' + defaultExpressRegExpString, method: 'PATCH', isCustomAction: false},
        {url: '/v1/users/:userId' + defaultExpressRegExpString, method: 'DELETE', isCustomAction: false},
        {url: '/v1/users/:userId' + defaultExpressRegExpString + '/hire', method: 'POST', isCustomAction: true},
        {url: '/v1/users/hire', method: 'POST', isCustomAction: true},
        {url: '/v1/users/:userId' + defaultExpressRegExpString + '/home-address', method: 'GET', isCustomAction: true},
        {url: '/v1/users/home-address', method: 'GET', isCustomAction: true},
        {url: '/v1/users/:userId' + defaultExpressRegExpString + '/send-email', method: 'POST', isCustomAction: true},
        {url: '/v1/users/look-up', method: 'POST', isCustomAction: true},
      ];

      let endpoints = prorestify.endpoints.map((endpoint) => _.pick(endpoint, 'url', 'method', 'isCustomAction'));
      should(endpoints.length).eql(expectedRoutes.length);
      should(endpoints).containEql(expectedRoutes[0]);
      // Leave only users endpoints for the rest of the test to check that customActions come first
      expectedRoutes.splice(0, 1);
      expectedRoutes.forEach((route) => {
        should(endpoints).containEql(route);
      });
      // Check that all custom actions go first
      should(_.findIndex(endpoints, (e) => !e.isCustomAction)).eql(6);
    });
  });

  describe('Routes Declaration', () => {
    let app;
    const registerRouteFunctions = ['get', 'post', 'put', 'patch', 'delete'];
    beforeEach(() => {
       app = express(); //Start fresh
       registerRouteFunctions.forEach((key) => spies[key] = sandbox.spy(app, key));
    });

    afterEach(() => {
      spies.get.restore();
      registerRouteFunctions.forEach((key) => spies[key].restore);
    });

    function getRegisteredUrlsFromSpy(spy) {
      let urls = [];
      for (let i = 0; i < spy.callCount; i++) {
        let url = spy.getCall(i).args[0];
        // Express adds some auxiliary routes. Ignore anything unrelated
        if (/^\/v[0-9]+/.test(url)) {
          urls.push(spy.getCall(i).args[0]);
        }
      }
      return urls;
    }

    function getRegisteredUrlsFromSpies() {
      let registeredUrls = {};

      registerRouteFunctions.forEach((key) => registeredUrls[key] = getRegisteredUrlsFromSpy(spies[key]));

      return registeredUrls;
    }

    it('should assign the routes to express', () => {
      let prorestify = new ProRESTify();
      prorestify.router(app);

      let expectedRoutes = {
        get: [
          '/v1/camel-case',
          '/v1/users',
          '/v1/users/:userId' + defaultExpressRegExpString,
          '/v1/users/home-address',
          '/v1/users/:userId' + defaultExpressRegExpString + '/home-address'
        ],
        post: [
          '/v1/users',
          '/v1/users/hire',
          '/v1/users/:userId' + defaultExpressRegExpString + '/hire',
          '/v1/users/:userId' + defaultExpressRegExpString+ '/send-email',
          '/v1/users/look-up',
          '/v1/users/:userId' + defaultExpressRegExpString + '/children/:childId' + defaultExpressRegExpString + '/friends'
        ],
        put : ['/v1/users/:userId' + defaultExpressRegExpString],
        patch: ['/v1/users/:userId' + defaultExpressRegExpString],
        delete: ['/v1/users/:userId' + defaultExpressRegExpString]
      };

      let registeredUrls = getRegisteredUrlsFromSpies();
      _.keys(expectedRoutes).forEach((key) => {
        should(registeredUrls[key].sort()).eql(expectedRoutes[key].sort());
      });

    });

    it('should assign routes with custom actions always first', () => {
      let prorestify = new ProRESTify();
      prorestify.router(app);

      let expectedCustomActionRoutes = {
        get: [
          '/v1/users/home-address',
          '/v1/users/:userId' + defaultExpressRegExpString+ '/home-address'
        ],
        post: [
          '/v1/users/hire',
          '/v1/users/:userId' + defaultExpressRegExpString + '/hire',
          '/v1/users/:userId' + defaultExpressRegExpString + '/send-email',
          '/v1/users/look-up'
        ]
      };

      let registeredUrls = getRegisteredUrlsFromSpies();
      _.keys(expectedCustomActionRoutes).forEach((key) => {
        let length = expectedCustomActionRoutes[key].length;
        let subArray = registeredUrls[key].slice(0, length);
        should(subArray.sort()).eql(expectedCustomActionRoutes[key].sort());
      });
    });

    it('should apply the provided id validation regexp', () => {
      let prorestify = new ProRESTify({
        idRegExp: /\w+/
      });
      prorestify.router(app);

      let expectedCustomActionRoutes = {
        get: [
          '/v1/users/home-address',
          '/v1/users/:userId(\\w+)/home-address'
        ],
        post: [
          '/v1/users/hire',
          '/v1/users/:userId(\\w+)/hire',
          '/v1/users/:userId(\\w+)/send-email',
          '/v1/users/look-up'
        ]
      };

      let registeredUrls = getRegisteredUrlsFromSpies();
      _.keys(expectedCustomActionRoutes).forEach((key) => {
        let length = expectedCustomActionRoutes[key].length;
        let subArray = registeredUrls[key].slice(0, length);
        should(subArray.sort()).eql(expectedCustomActionRoutes[key].sort());
      });

      should(stubs.winstonInfo.callCount).eql(14);
    });

    describe('Validation, middleware and controllers', () => {

      function getRegisteredHandlersFromSpy(method, spy) {
        let handlers = {};
        for (let i = 0; i < spy.callCount; i++) {
          let url = spy.getCall(i).args[0];
          // Express adds some auxiliary routes. Ignore anything unrelated
          if (/^\/v[0-9]+/.test(url)) {
            handlers[method + ' ' + url] = {
              middlewares: spy.getCall(i).args[2],
              validation: spy.getCall(i).args[4],
              controller: spy.getCall(i).args[6]
            };
          }
        }
        return handlers;
      }

      function getRegisteredHandlersFromSpies() {
        let registeredHandlers = {};
        registerRouteFunctions.forEach((key) => _.assign(registeredHandlers, getRegisteredHandlersFromSpy(key, spies[key])));
        return registeredHandlers;
      }

      it('should register the validation handler', () => {
        let prorestify = new ProRESTify();
        prorestify.router(app);
        let registeredHandlers = getRegisteredHandlersFromSpies();
        let validationHandler = registeredHandlers['post /v1/users'].validation;
        should(typeof validationHandler).eql('function');
      });

      describe('validation', () => {

        it('should call next with an error if the body does not follow validation schema', () => {
          let prorestify = new ProRESTify();
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let validationHandler = registeredHandlers['post /v1/users'].validation;

          let req = {body: {}};
          return BPromise.promisify(validationHandler)(req, {})
            .then(() => {
              throw new Error('Unexpected');
            })
            .catch((err) => {
              should(err.details).eql([
                {
                  message: '"username" is required',
                  path: 'username',
                  type: 'any.required',
                  context: { key: 'username' }
                },
                {
                  message: '"password" is required',
                  path: 'password',
                  type: 'any.required',
                  context: { key: 'password' }
                }
              ]);
            });
        });

        it('should call next without errors if the body does follow the validation schema ', () => {
          let prorestify = new ProRESTify();
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let validationHandler = registeredHandlers['post /v1/users'].validation;

          let req = {
            body: {username: 'testuser', password: 'whatever'}
          };
          return BPromise.promisify(validationHandler)(req, {})
          .then(() => {
            should(req.validatedData).deepEqual(req.body);
          });
        });

        it('should call next with an error if there is no validation schema ', () => {
          let prorestify = new ProRESTify();
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let validationHandler = registeredHandlers['put /v1/users/:userId([0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})'].validation;

          let req = {
            body: {username: 'testuser', password: 'whatever'}
          };
          return BPromise.promisify(validationHandler)(req, {})
          .then(() => {
            throw new Error('Unexpected');
          })
          .catch((err) => {
            should(err.details).eql([
              {
                message: '"username" is not allowed',
                path: 'username',
                type: 'object.allowUnknown',
                context: {
                  child: 'username',
                  key: 'username'
                } },
              {
                message: '"password" is not allowed',
                path: 'password',
                type: 'object.allowUnknown',
                context: {
                  child: 'password',
                  key: 'password'
                }
              }
            ]);
          });
        });

        it('should call next without errors if there is no validation schema and allowUnknown is set to true ', () => {
          let prorestify = new ProRESTify({
            allowUnknown: true
          });
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let validationHandler = registeredHandlers['put /v1/users/:userId([0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})'].validation;

          let req = {
            body: {username: 'testuser', password: 'whatever'}
          };
          return BPromise.promisify(validationHandler)(req, {})
          .then(() => {
            should(req.validatedData).deepEqual(req.body);
          });
        });
      });

      describe('middlewares', () => {
        it('should call the middleware', () => {
          let prorestify = new ProRESTify({
            allowUnknown: true
          });
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let middlewareHandler = registeredHandlers['patch /v1/users/:userId([0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})'].middlewares;

          let req = {
            body: {username: 'testuser', password: 'whatever'}
          };
          return BPromise.promisify(middlewareHandler)(req, {})
          .then(() => {
            should(req.wentHere).eql(true);
          });
        });

        it('should call multiple middlewares if there are more than one', () => {
          let prorestify = new ProRESTify({
            allowUnknown: true
          });
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let middlewareHandler = registeredHandlers['put /v1/users/:userId([0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})'].middlewares;

          let req = {
            body: {username: 'testuser', password: 'whatever'}
          };
          return BPromise.promisify(middlewareHandler)(req, {})
          .then(() => {
            should(req.nbMiddleware).eql(3);
          });
        });

        it('should throw an error if a middleware throws one', () => {
          let prorestify = new ProRESTify({
            allowUnknown: true
          });
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let middlewareHandler = registeredHandlers['delete /v1/users/:userId([0-9]+|[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})'].middlewares;

          let req = {
            body: {username: 'testuser', password: 'whatever'}
          };
          return BPromise.promisify(middlewareHandler)(req, {})
          .then(() => {
            throw new Error('Unexpected');
          })
          .catch(err => {
            should(err.message).eql('this is for the tests');
          });
        });
      });

      describe('controller', () => {
        it('should call the controller', (done) => {
          let prorestify = new ProRESTify({
            allowUnknown: true
          });
          prorestify.router(app);

          let registeredHandlers = getRegisteredHandlersFromSpies();
          let controllerHandler = registeredHandlers['post /v1/users'].controller;

          let req = {
            validatedData: {username: 'testuser', password: 'whatever'}
          };
          let res = {
            send: function(){ },
            json: function(resp){
              should(resp).eql({
                username: 'testuser',
                password: 'whatever',
                itWorks: true
              });
              should(this.statusTested).eql(true);
              done();
            },
            status: function(responseStatus) {
              should(responseStatus).eql(201);
              // This next line makes it chainable
              this.statusTested = true;
              return this;
            }
          };
          controllerHandler(req, res);
        });
      });
    });
  });

  describe('Real server', () => {
    const PORT = 8000;
    let baseUrl = 'http://localhost:' + PORT + '/';
    let app;
    let server;
    let prorestify;
    beforeEach(() => {
      server = null;
      app = express(); //Start fresh
      app.use(bodyParser.json());
      prorestify = new ProRESTify({allowUnknown: true});
      prorestify.router(app);
      server = app.listen(PORT);
    });

    afterEach(() => {
      server.close();
    });

    it('should return 201 and the expected body when calling POST /v1/users with the right body', () => {
      let opts = {
        json: {
          username: 'somebody',
          password: 'whatever'
        },
        resolveWithFullResponse: true
      };
      return rp.post(baseUrl + 'v1/users', opts)
      .then((resp) => {
        should(resp.statusCode).eql(201);
        should(resp.body).deepEqual({
          username: opts.json.username,
          password: opts.json.password,
          itWorks: true
        });
      });
    });

    it('should return 200 and the expected body when calling GET /v1/users', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.get(baseUrl + 'v1/users', opts)
      .then((resp) => {
        should(resp.statusCode).eql(200);
        should(resp.body).deepEqual({
          results: [{
            id: 1,
            username: 'example'
          }]
        });
      });
    });

    it('should return 200 and the expected body when calling GET /v1/users/1', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.get(baseUrl + 'v1/users/1', opts)
      .then((resp) => {
        should(resp.statusCode).eql(200);
        should(resp.body).deepEqual({
          id: 1,
          username: 'example'
        });
      });
    });

    it('should return 200 and the expected body when calling PUT /v1/users/1', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.put(baseUrl + 'v1/users/1', opts)
      .then((resp) => {
        should(resp.statusCode).eql(200);
        should(resp.body).deepEqual(true);
      });
    });

    it('should return an error when calling DELETE /v1/users/hire', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.del(baseUrl + 'v1/users/1', opts)
        .then(() => {
          throw new Error('Unexpected');
        })
        .catch(() => {
            // silent the error
        });
    });

    it('should return 200 and the expected body when calling POST /v1/users/hire', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.post(baseUrl + 'v1/users/hire', opts)
      .then((resp) => {
        should(resp.statusCode).eql(200);
        should(resp.body).deepEqual(true);
      });
    });

    it('should return 200 and the expected body when calling POST /v1/users/1/send-email', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.post(baseUrl + 'v1/users/1/send-email', opts)
      .then((resp) => {
        should(resp.statusCode).eql(200);
        should(resp.body).deepEqual(true);
      });
    });

    it('should return 200 and the expected body when calling POST /v1/users/look-up', () => {
      let opts = {
        json: true,
        resolveWithFullResponse: true
      };
      return rp.post(baseUrl + 'v1/users/look-up', opts)
      .then((resp) => {
        should(resp.statusCode).eql(200);
        should(resp.body).deepEqual(true);
      });
    });

    describe('Hooks', () => {
      let testUrl = baseUrl + 'v1/users';
      let testOpts = {
        json: {
          username: 'somebody',
          password: 'whatever'
        }
      };
      let endpoint;
      beforeEach(() => {
        endpoint = prorestify.endpoints.find((endpoint) => endpoint.url === '/v1/users' && endpoint.method === 'POST');
        spies.endpointMiddleware = sandbox.spy(endpoint.middlewares, '0');
        spies.endpointValidation = sandbox.spy(prorestify.options, 'validate');
      });

      it('should call beforeMiddlewares handler before middlewares', () => {
        return rp.post(testUrl, testOpts)
        .then(() => {
          should(spies.hookBeforeMiddlewares.callCount).eql(1);
          should(spies.endpointMiddleware.callCount).eql(1);
          should(spies.hookBeforeMiddlewares.calledBefore(spies.endpointMiddleware)).eql(true);
        });
      });

      it('should call beforeValidation after middlewared and before validation', () => {
        return rp.post(testUrl, testOpts)
        .then(() => {
          should(spies.endpointMiddleware.callCount).eql(1);
          should(spies.hookBeforeValidation.callCount).eql(1);
          should(spies.endpointValidation.callCount).eql(1);
          should(spies.endpointMiddleware.calledBefore(spies.hookBeforeValidation)).eql(true);
          should(spies.hookBeforeValidation.calledBefore(spies.endpointValidation)).eql(true);
        });
      });

      it('should call beforeController after validation and before controller', () => {
        return rp.post(testUrl, testOpts)
        .then(() => {
          should(spies.endpointValidation.callCount).eql(1);
          should(spies.hookBeforeController.callCount).eql(1);
          should(spies.endpointValidation.calledBefore(spies.hookBeforeController)).eql(true);
          // TODO: Check that if gets called before controller. COuld not spy on controller so far
        });
      });

      it('should call hookSuccess if no errors', () => {
        return rp.post(testUrl, testOpts)
        .then(() => {
          should(spies.hookSuccess.callCount).eql(1);
        });
      });

      it('should call hookError if there is an error', () => {
        let opts = {
          json: {
            username: null,
          }
        };
        return rp.post(testUrl, opts)
        .then(() => {
          throw new Error('unexpected');
        })
        .catch(() => {
          should(spies.hookFailure.callCount).eql(1);
        });
      });

    });
  });
});
